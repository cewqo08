/*
 *  Copyright (C) 2008 by Filip Brcic <brcha@gna.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"

#include <KApplication>
#include <KAction>
#include <KLocale>
#include <KActionCollection>
#include <KStandardAction>
#include <KFileDialog>
#include <KMessageBox>
#include <KIO/NetAccess>
#include <KSaveFile>
#include <QTextStream>
#include <KProgressDialog>
#include <QRegExp>
#include <KDebug>

MainWindow::MainWindow(QWidget *parent)
  : KXmlGuiWindow(parent),
    fileName(QString())
{
  tableWidget = new QTableWidget();
  tableWidget->setColumnCount(14);

  QStringList header;
  header << i18n("Title")
         << i18n("Full name")
         << i18n("State")
         << i18n("Type of presentation")
         << i18n("Topic")
         << i18n("Accomodation")
         << i18n("Fee")
         << i18n("Staying")
         << i18n("FEE")
         << i18n("Abstract")
         << i18n("Abstract Title")
         << i18n("E-mail")
         << i18n("Affiliation")
         << i18n("Desired Presentation (with abstract)");
  tableWidget->setHorizontalHeaderLabels(header);

  setCentralWidget(tableWidget);

  setupActions();
}

void MainWindow::setupActions()
{
  KAction *clearAction = new KAction(this);
  clearAction->setText(i18n("Clear"));
  clearAction->setIcon(KIcon("edit-clear"));
  clearAction->setShortcut(Qt::CTRL + Qt::Key_W);
  actionCollection()->addAction("clear", clearAction);
  connect(clearAction, SIGNAL(triggered(bool)),
	  this, SLOT(clear()));

  KAction *swapNamesAction = new KAction(this);
  swapNamesAction->setText(i18n("Swap names and surnames"));
  swapNamesAction->setIcon(KIcon("system-switch-user"));
  swapNamesAction->setShortcut(Qt::CTRL + Qt::Key_N);
  actionCollection()->addAction("swapNames", swapNamesAction);
  connect(swapNamesAction, SIGNAL(triggered(bool)),
           this, SLOT(swapNames()));

  KAction *unpackAffiliationAction = new KAction(this);
  unpackAffiliationAction->setText(i18n("Unpack the affiliation field"));
  unpackAffiliationAction->setIcon(KIcon("application-x-compress"));
  unpackAffiliationAction->setShortcut(Qt::CTRL + Qt::Key_U);
  actionCollection()->addAction("unpackAffiliation", unpackAffiliationAction);
  connect(unpackAffiliationAction, SIGNAL(triggered(bool)),
           this, SLOT(unpackAffiliation()));

  KStandardAction::quit(kapp, SLOT(quit()), actionCollection());
  KStandardAction::open(this, SLOT(openFile()), actionCollection());
  KStandardAction::save(this, SLOT(saveFile()), actionCollection());
  KStandardAction::saveAs(this, SLOT(saveFileAs()), actionCollection());
  KStandardAction::openNew(this, SLOT(newFile()), actionCollection());

  setupGUI();
}

void MainWindow::newFile()
{
  fileName.clear();
  clear();
}

void MainWindow::saveFileAs(const QString &outputFileName)
{
  KSaveFile file(outputFileName);
  file.open();

  QByteArray outputByteArray;
  QTextStream ts(&outputByteArray);
//   outputByteArray.append(textArea->toPlainText().toUtf8());
  for (int r=0; r < tableWidget->rowCount(); r++)
  {
    for (int c=0; c < tableWidget->columnCount(); c++)
    {
      if (c == 0)
        ts << tableWidget->item(r,c)->text().toUtf8();
      else
        ts << "%" << tableWidget->item(r,c)->text().toUtf8();
    }
    ts << endl;
  }

  file.write(outputByteArray);
  file.finalize();
  file.close();

  fileName = outputFileName;
}

void MainWindow::saveFileAs()
{
  saveFileAs(KFileDialog::getSaveFileName());
}

void MainWindow::saveFile()
{
  if (!fileName.isEmpty())
  {
    saveFileAs(fileName);
  }
  else
  {
    saveFileAs();
  }
}

void MainWindow::openFile()
{
  openFile(KFileDialog::getOpenFileName());
}

void MainWindow::openFile(const QString &inputFileName)
{
  QString tmpFile;
  QString currentLine;
  if (KIO::NetAccess::download(inputFileName, tmpFile, this))
  {
    QFile file(tmpFile);
    file.open(QIODevice::ReadOnly);
    QTextStream ts(&file);
    ts.setCodec("UTF-8");
    ts.setAutoDetectUnicode(true);
    ts.readLine(); ts.readLine(); // drop the first two lines as they are not data
    while (!ts.atEnd())
    {
      currentLine = ts.readLine();
      QStringList list = currentLine.split("%");
      int currentRow = tableWidget->rowCount();
      tableWidget->insertRow(currentRow);
      for (int i=0; i < list.size(); i++)
        tableWidget->setItem(currentRow, i, new QTableWidgetItem(list.at(i)));
    }
//     textArea->setPlainText(QTextStream(&file).readAll());
    fileName = inputFileName;

    KIO::NetAccess::removeTempFile(tmpFile);
  }
  else
  {
    KMessageBox::error(this, KIO::NetAccess::lastErrorString());
  }
}

void MainWindow::swapNames()
{
  KProgressDialog * pd = new KProgressDialog(this,
                                              i18n("Swapping first and last names"),
                                              i18n("Swapping first and last names"));
  pd->setAllowCancel(false);
  pd->progressBar()->setRange(0,tableWidget->rowCount()-1);
  pd->show();
  for (int r=0; r < tableWidget->rowCount(); r++)
  {
    pd->progressBar()->setValue(r);
    QString currentName = tableWidget->item(r, 1)->text();
    QStringList list = currentName.split(' '); // split the name in firstname and lastname
    // for those who have more than 2 words in fullname, treat the last word as lastname
    list.push_front(list.last());
    list.pop_back();
    tableWidget->item(r, 1)->setText(list.join(" "));
  }
  // resort the table by names
  tableWidget->sortItems(1);
}

void MainWindow::clear()
{
  tableWidget->clearContents();
  tableWidget->setRowCount(0);
}

void MainWindow::unpackAffiliation()
{
  tableWidget->insertColumn(12);
  tableWidget->insertColumn(12);
  tableWidget->insertColumn(12);
  tableWidget->insertColumn(12);
  tableWidget->insertColumn(12);
  tableWidget->insertColumn(12);

  QStringList header;
  header << i18n("Title")
         << i18n("Full name")
         << i18n("State")
         << i18n("Type of presentation")
         << i18n("Topic")
         << i18n("Accomodation")
         << i18n("Fee")
         << i18n("Staying")
         << i18n("FEE")
         << i18n("Abstract")
         << i18n("Abstract Title")
         << i18n("E-mail")
         << i18n("Institution") //12
         << i18n("Address") //13
         << i18n("Zip code") //14
         << i18n("City") //15
         << i18n("Country") //16
         << i18n("Phone") //17
         << i18n("Fax") //18
         << i18n("Desired Presentation (with abstract)");
  tableWidget->setHorizontalHeaderLabels(header);
// Institution: Faculty of Physics, Adam Mickiewicz UniversityAddress: ul. Umultowska 85, City: Poznan, Zip code: 61-614, Country: Poland, Phone: +48 61 8295 274, Fax: +48 61 8257 018
// Institution: Friedrich-Schiller-Universitaet Jena Theoretisch-Physikalisches InstitutAddress: Max-Wien-Platz 1, City: Jena, Zip code: D-07743, Country: Germany, Phone: , Fax:
  QRegExp rx("^Institution: (.*)Address: (.*), City: (.*), Zip code: (.*), Country: (.*), Phone: (.*), Fax:(.*)$");
  KProgressDialog * pd = new KProgressDialog(this,
                                              i18n("Unpacking affiliations"),
                                              i18n("Unpacking affiliations"));
  pd->setAllowCancel(false);
  pd->progressBar()->setRange(0,tableWidget->rowCount()-1);
  pd->show();
  for (int r=0; r < tableWidget->rowCount(); r++)
  {
    pd->progressBar()->setValue(r);
    QString currentAffiliation = tableWidget->item(r, 18)->text();
    kDebug() << "Row: " << r << " affiliation: " << currentAffiliation << endl;
    int pos = rx.indexIn(currentAffiliation);
    if (pos > -1)
      {
	kDebug() << "Matched: " << rx.capturedTexts() << endl;
	tableWidget->setItem(r,12,new QTableWidgetItem(rx.cap(1))); // institution
	tableWidget->setItem(r,13,new QTableWidgetItem(rx.cap(2))); // address
	tableWidget->setItem(r,14,new QTableWidgetItem(rx.cap(4))); // zip code
	tableWidget->setItem(r,15,new QTableWidgetItem(rx.cap(3))); // city
	tableWidget->setItem(r,16,new QTableWidgetItem(rx.cap(5))); // country
	tableWidget->setItem(r,17,new QTableWidgetItem(rx.cap(6))); // phone
	tableWidget->setItem(r,18,new QTableWidgetItem(rx.cap(7))); // fax
      }
    else
      kDebug() << "no match :(" << endl;
  }
}

