/*
 *  Copyright (C) 2008 by Filip Brcic <brcha@gna.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __MAINWINDOW_H__
#define __MAINWINDOW_H__

#include <KXmlGuiWindow>
#include <QTableWidget>

class MainWindow : public KXmlGuiWindow
{
  Q_OBJECT

  public:
    MainWindow(QWidget *parent=0);
    void openFile(const QString &inputFileName);

  private:
    QTableWidget *tableWidget;
    void setupActions();
    QString fileName;

  private slots:
    void newFile();
    void openFile();
    void saveFile();
    void saveFileAs();
    void saveFileAs(const QString &outputFileName);
    void swapNames();
    void clear();
    void unpackAffiliation();
};

#endif /* __MAINWINDOW_H__ */
