/*
 *  Copyright (C) 2008 by Filip Brcic <brcha@gna.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <KApplication>
#include <KAboutData>
#include <KCmdLineArgs>
#include <KUrl>

#include "mainwindow.h"

int main (int argc, char *argv[])
{
  KAboutData aboutData("cewqo08", "cewqo08",
		       ki18n("CEWQO 2008"), "1.0",
		       ki18n("A simple application that parses CEWQO 2008 attendee list."),
		       KAboutData::License_GPL_V3,
		       ki18n("Copyright (c) 2008 Filip Brcic <brcha@gna.org>"));
  KCmdLineArgs::init(argc, argv, &aboutData);

  KCmdLineOptions options;
  options.add("+[file]", ki18n("Document to open"));
  KCmdLineArgs::addCmdLineOptions(options);

  KApplication app;

  MainWindow *window = new MainWindow();
  window->show();

  return app.exec();
}
